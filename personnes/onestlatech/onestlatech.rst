.. index::
   pair: Comptes Twiter ; https://twitter.com/OnEstLaTech


.. _onestlatech:

==============
OnEstLaTech
==============

.. seealso::

   - https://twitter.com/OnEstLaTech
   - https://mastodon.social/@onestlatech
   - https://github.com/onestlatech
   - https://github.com/onestlatech/onestlatech.github.io
   - https://onestla.tech/

.. figure:: logo_onestlatech.png
   :align: center

.. contents::
   :depth: 3



Genèse
=======

.. seealso::

   - https://onestla.tech/page/a-propos/
   - https://soundcloud.com/podcastecho/e41-appel-des-travailleuses-et-travailleurs-du-numerique-pour-une-autre-reforme-des-retraites


Au cours du week-end du 6 et 7 décembre 2019, plus d’une centaine d’actrices et
d’acteurs du numérique se sont organisés, et se sont mis d'accord pour publier
cet appel.

Ce texte appelle les travailleuses et travailleurs du numérique à se mobiliser
pour dire non à la réforme des retraites du gouvernement et pour proposer une
alternative : réduire le temps de travail en mettant la technologie au service
du bien commun. Pour en savoir plus, `écoutez ce podcast`_.

Nous contacter::

    onestlatech@protonmail.com

.. _`écoutez ce podcast`: https://soundcloud.com/podcastecho/e41-appel-des-travailleuses-et-travailleurs-du-numerique-pour-une-autre-reforme-des-retraites


Podcast
=========

.. seealso::

   - https://onestla.tech/page/a-propos/
   - https://soundcloud.com/podcastecho/e41-appel-des-travailleuses-et-travailleurs-du-numerique-pour-une-autre-reforme-des-retraites


Dans cet épisode, on parle de l'appel des travailleuses et travailleurs du
numérique pour une autre réforme des retraites : onestla.tech/ avec quelques uns
des contributeurs et signataires du texte qui se sont réunis à Paris lors des
ApiDays, entre parisiens, auvergnats ou nordistes :

- Kévin Dunglas, développeur, contributeur open source, fondateur et coopérateur
  les-tilleuls.coop/
- Mehdi Medjaoui, entrepreneur, fondateur de gdpr.dev/ et organisateur des
  évènements ApiDays
- Christophe Robillard, développeur et agiliste indépendant
- Hélène Maitre, agiliste et coopératrice fairness.coop/

.. figure:: podcast_onestlatech.png
   :align: center

   https://soundcloud.com/podcastecho/e41-appel-des-travailleuses-et-travailleurs-du-numerique-pour-une-autre-reforme-des-retraites


Pour aller plus loin
========================

.. seealso::

   - https://discord.gg/se3PnEr
   - https://greve.cool/
   - https://github.com/noelmace/widget-engreve

Nous suivre sur Twitter et sur Mastodon
Afficher son soutien sur les réseaux sociaux en ajoutant ✊ #OnEstLaTech à
son pseudo et utiliser l’avatar du mouvement

Pour se coordonner et agir : `rejoindre notre Discord`_
S'informer sur la grève : `La grève, c'est cool`_
Mettre votre site en grève : `le Widget En Grève, strike-js`_

.. _`rejoindre notre Discord`: https://discord.gg/se3PnEr
.. _`La grève, c'est cool`: https://greve.cool/
.. _`le Widget En Grève, strike-js`:  https://github.com/noelmace/widget-engreve

L'appel
=========

.. seealso::

   - https://onestla.tech/


Publications
=============

.. seealso::

   - https://onestla.tech/publications/


Ressources
===========

.. seealso::

   - https://onestla.tech/page/ressources/


Tracts
=======


:download:`https://onestla.tech/tracts/tract-a5-recto-verso.pdf`
