.. index::
   pair: Attac ; Répartition

.. _reforme_par_repartition_attac:

============================================================================
**Un autre projet de retraites est possible !** (24 janvier 2020 par Attac)
============================================================================

.. seealso::

   - https://france.attac.org/nos-publications/notes-et-rapports/article/rapport-un-autre-projet-de-retraites-est-possible
   - https://france.attac.org/IMG/pdf/projet_retraites6.pdf


.. contents::
   :depth: 3

Introduction
==============


Refuser un tel projet ne vaut donc pas acceptation du statu quo.

Attac et la Fondation Copernic considèrent que le système de retraites actuel
doit être notablement amélioré en revenant notamment sur les mesures régressives
prises par les gouvernements précédents mais aussi en assurant de nouveaux droits.

Il s’agit alors de modifier l’organisation actuelle du système avec l’objectif
d’harmoniser par le haut les différents régimes pour construire un droit universel
à la retraite sur la base de quatre principes :

- tenir compte des spécificités professionnelles ;
- combattre les inégalités, en particulier entre les femmes et les hommes, en
  assurant une redistribution par des dispositifs de solidarité renforcés ;
- garantir un montant de pension qui permette un maintien du niveau de vie lors
  du départ en retraite ;
- partir assez tôt en retraite pour que cette période de vie puisse être
  vécue sans incapacité.
