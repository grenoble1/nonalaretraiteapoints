.. index::
   pair: Manifestation interprofessionnelle; jeudi 9 janvier 2020

.. _manif_2020_01_09:

==================================================================================================================
**Manifestation interprofessionnelle** du jeudi 9 janvier 2020
==================================================================================================================

.. seealso::

   - https://38.demosphere.net/rv/9
   - https://38.demosphere.net/?a=1577836800#d1-1
