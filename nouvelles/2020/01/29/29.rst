.. index::
   pair: 2020-01-29 ; Retraites

.. _luttes_retraites_2020_01_29:

===========================================
Mercredi 29 janvier 2020
===========================================

.. seealso::

   - https://38.demosphere.net/rv/218
   - http://ici-grenoble.org/infos/la-une.php?id=8595
   - http://www.ici-grenoble.org/agenda/evenement.php?id=10042

.. toctree::
   :maxdepth: 4


   tract_cnt
   tract_intersyndical
   manif_interprofessionnelle
