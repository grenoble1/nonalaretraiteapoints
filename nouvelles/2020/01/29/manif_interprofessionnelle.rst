.. index::
   pair: Manifestation interprofessionnelle; mercredi 29 janvier 2020

.. _manif_2020_01_29:

==================================================================================================================
**Manifestation interprofessionnelle** du mercredi 29 janvier 2020
==================================================================================================================

.. seealso::

   - https://38.demosphere.net/rv/218
   - https://38.demosphere.net/?a=1577836800#d1-1
   - http://ici-grenoble.org/infos/la-une.php?id=8595
   - http://www.ici-grenoble.org/agenda/evenement.php?id=10042


.. contents::
   :depth: 3


Manifestation interluttes contre la réforme des retraites et contre "le président des ultra-riches"
========================================================================================================

.. seealso::

   - http://ici-grenoble.org/infos/la-une.php?id=8595
   - https://www.lepotsolidaire.fr/pot/solidarite-financiere


Vous ne voulez pas de la retraite par points, première étape vers la retraite
par capitalisation ?

Vous ne voulez pas des BlackRock et autres vautours de la finance ?

Uune nouvelle manifestation interluttes s'organise contre la réforme des retraites,
mais aussi **la présidence des ultra-riches**, le renforcement des inégalités, la
sape des services publics, la répression des luttes sociales ou encore l'inaction climatique.

À 14h

De la gare de Grenoble

Voici également le lien vers la cagnotte_ de soutien aux salarié-e-s grévistes,
mise en place par le syndicat InfoCom-CGT.

.. _cagnotte: https://www.lepotsolidaire.fr/pot/solidarite-financiere
