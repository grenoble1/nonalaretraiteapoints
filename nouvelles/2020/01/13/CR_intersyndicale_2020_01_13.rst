

.. _retraites_2020_01_13_intersyndicale:

====================================================================
Compte rendu de la réunion intersyndicale du 13/01/2020 à Grenoble
====================================================================

.. seealso::

   - https://38.demosphere.net/rv/145
   - https://38.demosphere.net/rv/128


.. contents::
   :depth: 3


Compte rendu de la réunion intersyndicale du 13/01/2020
============================================================

Présents : CGT, FO, CFE-CGC-FSU-SOLIDAIRES, CNT, FSU, UNEF
Ordre du jour : organisation manifestation 14 et 16 janvier 2020

Discussion : En Isère nous avions prévue une manifestation le 14 janvier 2020
en journée pour répondre aux demandes de l’intersyndicale nationale concernant
une journée de grève et de manifestation de jour là et une marche aux flambeaux
le 16 janvier 2020 à 17h.

L'intersyndicale nationale réunie ce samedi a fiait le choix de changer la date
en portant le 16 janvier 2020 comme le temps fort de la semaine,
Les affaire étant été lancées en Isère avec des milliers de tracts distribuées
depuis ce week-end. Il n'est donc pas sérieux de modifier ces événements et
d'envoyer des messages contradictoires, d’où la proposition de répondre tout
de même à l'appel en faisant du jeudi 16 une nouvelle journée de grève et de
manifestations dans tout le territoire.


Décisions
==========

Aussi, pour Grenoble, la manif_ partira de la gare sncf avec un rendez-vous fixé
à 14h, direction Félix Poulat pour prise de parole des OS puis départ pour la
Préfecture aux environs de 17h pour défilé `retraites aux flambeaux`_.

L’intersyndicale décident de mettre en avant **les spécificités des femmes** dans le
cadre de la réforme systémique de retraite à points et les conséquences
aggravantes en tête de cortège, soit derrière le kangoo de la CGT, derrière la
banderoles de tête intersyndicale qui sera composés de militiantes
syndicaux,

Pour Bougoin et Roussillon, les intersyndicales locales décident en ce moment
des initiatives à tenir le

16/01/2020, soit

Roussillon : manif 10h parking ex Géant Casino RN7 à Salaise sur Sanne
Bourgoin Jallieu : 10h Pont St Michel
Prochaine réunion intersyndicale : vendredi 17 janvier à 12h30 (locaux CGT)

.. _manif: https://38.demosphere.net/rv/145
.. _`retraites aux flambeaux`: https://38.demosphere.net/rv/128
