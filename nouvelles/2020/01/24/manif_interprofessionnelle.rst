.. index::
   pair: Manifestation interprofessionnelle; vendredi 24 janvier 2020

.. _manif_2020_01_24:

==================================================================================================================
**Manifestation interprofessionnelle** du vendredi 24 janvier 2020
==================================================================================================================

.. seealso::

   - https://38.demosphere.net/rv/166
