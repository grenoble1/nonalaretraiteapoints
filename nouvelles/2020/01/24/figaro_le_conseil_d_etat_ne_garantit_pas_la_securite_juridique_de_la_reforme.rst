.. index::
   pair: Article ; Avis très sévère du Conseil d'État: Le conseil d'Etat ne garantit pas la sécurité juridique de la réforme

.. _figaro_avis_tres_severe_2020_01_20:

=========================================================================================================================
**Avis très sévère du Conseil d'État: Le conseil d'Etat ne garantit pas la sécurité juridique de la réforme** (Figaro)
=========================================================================================================================

.. seealso::

   - :ref:`dossier_figaro_avis_tres_severe_2020_01_24`
