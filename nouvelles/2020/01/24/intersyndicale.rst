.. index::
   pair: Communiqué ; Nos mobilisations seront victorieuses !

.. _communique_intersyndicale_2020_01_24:

==================================================================================================================
**Nos mobilisations seront victorieuses !** communiqué de l'intersyndicale nationale le vendredi 24 janvier 2020
==================================================================================================================

.. seealso::

   - https://38.demosphere.net/rv/166


.. contents::
   :depth: 3


Format PDF
==========

:download:`Communiqué du vendredi 24 janvier 2020 <intersyndicale/Communique_intersyndical_2020_01_24.pdf>`

Le texte
=============

Nos mobilisations seront victorieuses !
=========================================

Jeudi 23 janvier 2020, partout dans le pays, à l’appel de nos organisations
syndicales, des manifestations éclairées par des flambeaux ont permis à beaucoup
de continuer à exiger le retrait du projet de réforme des retraites.

Dans le même temps, la grève se poursuit dans de nombreuses professions qui se
mobilisent et multiplient les actions de dépôts des outils de travail dans des
lieux symboliques ; **les femmes organisent partout des initiatives pour visibiliser
qu’elles sont les grandes perdantes de ce projet**.

Cette journée du 24 janvier 2020 s’annonce d’ores et déjà comme un succès et
loin de s’essouffler la mobilisation par la grève et par les manifestations
poursuit son ancrage interprofessionnel porté par un soutien large et massif
de la population.

Tout au long des jours prochains sur tout le territoire de nombreuses actions
sont déjà programmées, interpellations de parlementaires, soirées de soutiens,
débats, meetings, manifestations, etc.

Notre mouvement s’étend et se renforce dans la durée avec énergie et volonté.

L’intersyndicale réclame plus que jamais le retrait du projet de réforme pour
ouvrir des négociations constructives afin d’améliorer le régime actuel pour
tous les secteurs professionnels et toutes les générations.

Les organisations syndicales CGT, FO, FSU, Solidaires, FIDL, MNL, UNL et UNEF
appellent à faire du :ref:`mercredi 29 janvier 2020 <tract_retraites_2020_01_29>`,
veille de la conférence de financement, une journée massive de grève et de
manifestations interprofessionnelles et intergénérationnelles.

Elles appellent à poursuivre le renforcement et l’élargissement de la mobilisation
en multipliant les initiatives les jeudi et vendredi qui suivent notamment avec
des retraites aux flambeaux, des dépôts d’outils de travail, des assemblées
générales, des initiatives en direction de la population.

Nos organisations décident de se revoir le 29 janvier 2020 pour décider ensemble
des suites.

Paris, 24 janvier 2020
