.. index::
   pair: Article ; Un système de retraite cybernétique

.. _systeme_cybernetique_2020_01_24:

==========================================================================================================
**Réforme des retraites : l’hypothèse cybernétique** par Gérald Sédrati-Dinet (https://twitter.com/gibus)
==========================================================================================================

.. seealso::

   - https://pascontent.sedrati-dinet.net/index.php/post/2020/01/24/Un-systeme-de-retraite-cybernetique
   - https://lundi.am/Reforme-des-retraites-l-hypothese-cybernetique
   - https://twitter.com/gibus


.. contents::
   :depth: 3


Définition
===========

La **cybernétique** n’est pas, comme on voudrait l’entendre exclusivement, la
sphère séparée de la production d’informations et de la communication,
un espace virtuel qui se surimposerait au monde réel.

Elle est bien plutôt un monde autonome de dispositifs confondus avec le projet
capitaliste en tant qu’il est un projet politique, une gigantesque “machine abstraite”
faite de machines binaires effectuées par l’Empire, forme nouvelle de la
souveraineté politique, il faudrait dire une machine abstraite qui s’est
fait machine de guerre mondiale.


Introduction
============

Suite à des discussions avec des collègues sur les raisons et la manière dont
nous pourrions nous opposer au projet de loi sur la retraite universelle, je
me suis attelé à un exercice que je voulais absolument éviter – j’expliquerai
en détail pourquoi – : l’analyse des projets de textes législatifs rendus
publics vendredi 10 janvier 2020.

Il s’agit en effet de répondre à la question des raisons qui peuvent pousser à
s’opposer à ce projet sur la retraite universelle.
Au-delà des positions exprimées par les opposants, des déclarations du gouvernement,
des discussions de comptoir des commentateurs médiatiques et des éclairs de
lucidité entendus au comptoir:

- ce projet constitue-t-il bien une régression en terme de qualité de vie ?
- Pour quelques malchanceux seulement ?
- Ou pour toute la population française, y compris et surtout pour les
  générations à venir ?
- Et si oui, comment cela se traduit-il 

Calendrier
============

Et ce n’est pas tout, puisque ce projet de loi sur la retraite universelle vient
à peine de commencer son parcours législatif. Le texte dont on dispose aujourd’hui
est en effet celui validé par le Conseil d’État.
Il vient tout juste d’être présenté en Conseil des ministres. Il lui reste encore
à être déposé à l’Assemblée nationale et au Sénat, qui doivent chacun l’examiner
et le voter en commission spéciale puis en séance plénière — l’examen à l’Assemblée
est prévu à partir du 17 février 2020 pour un vote en plénière envisagé au plus
tard début mars, avant l’examen et le vote du Sénat, puis éventuellement la
convocation d’une Commission mixte paritaire, composée de députés et de sénateurs,
si les deux chambres ne sont pas exactement d’accord sur chacune des mesures du
texte et enfin le vote par les deux chambres si elles parviennent à un accord ou
une décision finale de l’Assemblée qui a le dernier mot en cas de désaccord avec
le Sénat. La promulgation du texte final est envisagée par le gouvernement
avant l’été. Mais attention, ce ne sont là que des prévisions de calendrier
qui peuvent encore être bouleversées à tout moment.


Le système de retraite par répartition
========================================

Le système de **retraite par répartition**, tel que mis en place en France selon le
programme du Conseil national de la Résistance, repose sur le prélèvement d’une
part de la survaleur produite par les travailleurs actuels, qui est reversée
aux anciens travailleurs qui ont cessé leur activité. Il suppose que,
quantitativement, le travail actuel génère suffisamment de valeur pour qu’une
partie échappe à la seule logique du Capital et soit distribuée à ceux qui ont
tout au long de leur vie participé, de par leur travail, à cette même logique.


Il importe peu de savoir qui a raison sur ce point, si le système de retraite
pourrait ou non être garanti dans son fonctionnement actuel. Pourquoi ?
Parce que si le système de retraite est en crise, c’est avant tout parce le
Capital est lui-même en crise. L’aspect par lequel cette crise du Capital se
présente de manière de plus en plus indéniable pour tout le monde est sans aucun
doute la crise climatique et environnementale.
On ne sait pas si cela sera pour nous, pour nos enfants ou nos petits enfants,
mais il est maintenant certain que la vie sur Terre n’est plus capable de se
maintenir si elle persiste dans la même forme. Cette forme de vie qui s’étend
toujours plus, tant spatialement – au point de couvrir le globe entier –, qu’en
profondeur – jusqu’à gouverner nos corps et nos esprits dans ce qu’ils ont de
plus intime.

**Cette forme de vie est celle qu’engendre le monde du Capital.
Et l’on sait que la planète ne supporte déjà plus cette forme de vie**.

Quel intérêt peut-il y avoir à s’escrimer pour aménager un projet de loi
perpétuant cette manière d’habiter la Terre, lorsque l’on sait que c’est
justement la perpétuation de celle-là qui condamne celle-ci ?

Alors que le nouveau régime de retraite ne versera pas de pensions avant 2037,
les projections les moins alarmistes quant au changement climatique prévoient
que d’ici une dizaine d’années, la hausse des températures entrainera une
explosion de la mortalité.

Il ne s’agit plus de défendre tel ou tel système de retraite venant récompenser
une vie suspendue à l’exigence du Travail – au sens où ce dernier est la
substance du Capital. Puisque c’est en travaillant toujours plus — au sens d’un
accroissement de la dépense abstraite valorisant le Capital – que s’amplifie
l’emprise destructrice que nous avons sur la planète.

Le problème de vivre du Travail se déplace du plan où il était question de
savoir à quel âge et sous quelles conditions on sortirait du Travail grâce
à la retraite, vers le plan où ce qui importe est de savoir comment vivre
tout à fait en dehors d’une logique vouée à rendre l’environnement invivable.

Non plus : comment sortir en théorie d’un monde selon les aménagements qu’il
nous accorde pour que toute notre vie reste claustrée à l’intérieur de ce
même monde ? Mais : comment pratiquement repousser ce monde en perdition
hors de nous pour atteindre les mondes au-dehors ?

Il faudrait ajouter à ça qu’avec l’évolution du Capital ces dernières décennies,
ayant généré, entre autres, une crise sociale où le chômage est devenu structurel,
toute discussion sur un départ à la retraite à 62 ans ou à un âge pivot pour
lequel on se battrait afin de le conserver à 64 ans plutôt que 65, 66, voire 67,
revient à un pur bavardage.
**Lorsque le monde du Travail vous rejette sitôt passé la cinquantaine** et qu’il
accueille la jeunesse au compte-goutte et dans des conditions de plus en plus
précaires, revendiquer 69 aurait au moins le charme d’illustrer que ces
discussions vont cul par-dessus tête.

Au final, on a déjà bien compris que puisque la retraite est intimement liée
au mode de vie du Capital, l’opposition au projet de loi sur les retraites
dépasse largement ce dernier pour viser directement le système du Capital
sur lequel il repose.
