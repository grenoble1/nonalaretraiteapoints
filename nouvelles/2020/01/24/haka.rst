.. index::
   pair: 2020-01-24 ; Haka

.. _haka_2020_01_24:

=======================================================
Vidéo Haka en tutus devant la préfecture de Grenoble
=======================================================

.. seealso::

   - https://twitter.com/GreenStuffDJ/status/1220748580123762688?s=20
   - :ref:`manif_2020_01_24`


.. figure:: haka/haka_grenoble.png
   :align: center

   https://twitter.com/GreenStuffDJ/status/1220748580123762688?s=20
