.. index::
   pair: Nouvelles ; 2019-12-03

.. _luttes_retraites_2019_12_03_dossier:

============================================================================================
Mardi 3 décembre 2019 : dossier "Il ne faut pas réformer les retraites" par Gilles Raveaud
============================================================================================

.. seealso::

   - :ref:`dossier_ne_pas_reformer_2019_12_03`
