.. index::
   pair: Manifestation interprofessionnelle ; jeudi 5 décembre 2020

.. _manif_2019_12_05:

===========================================================================
**Manifestation interprofessionnelle** à Grenoble le jeudi 5 décembre 2020
===========================================================================

.. seealso::

   - https://38.demosphere.net/rv/129
