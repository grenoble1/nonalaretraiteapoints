.. index::
   pair: Mon premier jour de grève ; https://twitter.com/peioroth64

.. _info_retraites_2019_11_25:

===============================================
**Mon premier jour de grève** par @peioroth64
===============================================

.. seealso::

   - https://twitter.com/peioroth64
   - https://medium.com/@pierreroth64/mon-premier-jour-de-gr%C3%A8ve-dfaa3bbec0ea
   - :ref:`changer_systeme_2020_01_18`
   - :ref:`onestlatech`


.. contents::
   :depth: 3


L’essentiel: **l’écologie et le social**
==========================================

L’actualité, mes lectures, mes sensations, tout converge… J’ai pris conscience
que tout est lié, entremêlé.

- On ne peut pas dissocier l’écologie et le social
- On ne peut pas dire que la croissance est découplée de l’impact écologique
- On ne peut pas dire et faire l’inverse de ce qu’on a dit
- On ne peut pas tout financiariser
- Je ne VEUX plus que quelques-uns mettent en péril le reste du monde en baignant
  dans des piscines de dollars

Face à l’effondrement planétaire en cours, nous ne pouvons pas rester sans rien
faire. Le papa que je suis n’a pas le choix. Pour essayer d’apaiser mes nuits,
je dois entrer en action pour pouvoir regarder mes enfants en face, pour cette
infirmière dévoué, pour ce prof qui bricole pour “tenir” encore, etc…

Alors chers amis, cadres du privé, Macron et son gouvernement va essayer de
nous enfumer encore et encore: il va pointer les cheminots et la défense de
leur régime mais NON NON et NON !!! Le problème n’est pas là.

Le 5 décembre 2019 je ne vais pas défendre un régime en particulier, je vais
protester contre ce monde financiarisé qui écrase tout sur son passage pour
le bien-être de quelques-uns.

Il y a un “pognon de dingue” sur cette planète. Et bien par notre action
déterminée et concertée, il faut maintenant qu’on impose à ces profiteurs
une nouvelle donne, pour les gens, pour le bien commun, pour la planète et le vivant.


C’est donc décidé, le 5 décembre, j’y serai !
=================================================

Le 5 décembre, je ferai donc le premier jour de grève de ma vie pour soutenir:

- les urgentistes
- les pompiers
- les cheminots
- les agriculteurs
- les étudiants
- les routiers
- les chômeurs
- les commerçants
- les postiers
- les avocats
- les gilets jaunes
- les agents de la fonction publique
- les policiers
- et plus généralement, tous ceux qui souffrent de par ce système de merde

J’irai crier ma rage pour nos enfants, pour la planète, pour le vivant.
