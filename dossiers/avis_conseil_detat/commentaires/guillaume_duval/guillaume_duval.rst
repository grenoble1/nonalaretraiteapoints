.. index::
   pair: Guillaume Duval ; Conseil d'Etat

.. _duval_2020_01_25:

=========================================
Commentaire de Guillaume Duval
=========================================

.. seealso::

   - https://twitter.com/gduval_altereco
   - https://twitter.com/gduval_altereco/status/1220963923706417152?s=20
   - https://twitter.com/gduval_altereco/status/1220823899874779137?s=20

Commentaire du vendredi 24 janvier 2020
=========================================


1/2 Merci au Conseil d'État d'avoir souligné combien cette  #reformedesretraites
ne tenait pas la route, combien les projections financières étaient lacunaires,
combien les 29 ordonnances privent le Parlement de tout contrôle réel sur la
loi réforme

.. figure:: commentaire_guillaume_duval_2020_01_24.png
   :align: center

   https://twitter.com/gduval_altereco/status/1220823899874779137?s=20


Commentaire 1 du samedi 25 janvier 2020 (7:57 AM · 25 janv. 2020·Twitter Web App)
==================================================================================


1/2 #reformedesretraites Après la publication de l'étude d'impact et l'avis du
Conseil d'Etat je ne vois vraiment pas comment un démocrate sincère,
un syndicaliste sérieux pourrait défendre désormais une autre position que
l'exigence du retrait immédiat de ce projet de loi bâclé.


.. figure:: commentaire_guillaume_duval_2020_01_25_1.png
   :align: center

   https://twitter.com/gduval_altereco/status/1220963923706417152?s=20

Commentaire 2 du samedi 25 janvier 2020 (7:57 AM · 25 janv. 2020·Twitter Web App)
==================================================================================

2 Toute autre attitude revient qu'on le veuille ou non à cautionner un coup
de force antidémocratique visant manifestement en priorité à faire baisser les
#Retraites retraites futures.

.. figure:: commentaire_guillaume_duval_2020_01_25_2.png
   :align: center

   https://twitter.com/gduval_altereco/status/1220963924864094208?s=20
