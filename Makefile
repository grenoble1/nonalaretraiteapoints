# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = Tutopython
SOURCEDIR     = .
BUILDDIR      = _build

THIS_MAKEFILE := $(lastword $(MAKEFILE_LIST))

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
	@echo " "
	@echo "Targets:"
	@echo " "
	@echo "- make check_all"
	@echo "- make req"
	@echo "- make updatetools"
	@echo "- make update"
	@echo " "


check_all:
	pre-commit run --all-files

req:
	poetry env info --path
	poetry show --tree
	poetry check
	poetry export -f requirements.txt --without-hashes  > requirements.txt
	cat requirements.txt

update:
	poetry update
	@$(MAKE) -f $(THIS_MAKEFILE) req
	git diff requirements.txt

updatetools:
	pre-commit autoupdate
	gitmoji --update
	git status

.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
