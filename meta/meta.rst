.. index::
   ! meta-infos


.. _luttes_meta_infos:

=====================
Meta infos
=====================

.. seealso::

   - https://cntvignoles.frama.io/meta/

.. contents::
   :depth: 3


Gitlab project
================

.. seealso::

   - https://framagit.org/grenoble1/nonalaretraiteapoints


Issues
--------

.. seealso::

   - https://framagit.org/grenoble1/nonalaretraiteapoints/-/boards


root directory
===============

::

    $ ls -als

::


    $ ls -als
    total 156
    4 drwxr-xr-x 10  4096 nov.  30 18:21 .
    4 drwxr-xr-x  4  4096 nov.  30 18:19 ..
    4 drwxr-xr-x  3  4096 nov.  30 18:19 alternatives
    4 -rwxr-xr-x  1  3804 nov.  30 18:21 conf.py
    76 -rw-r--r--  1 76971 nov.  30 18:19 contre_les_tyrans.png
    4 drwxr-xr-x  5  4096 nov.  30 18:19 dossiers
    4 -rw-r--r--  1    93 oct.  27 18:29 feed.xml
    4 drwxr-xr-x  3  4096 nov.  30 18:19 femmes
    4 drwxr-xr-x  8  4096 nov.  30 18:21 .git
    4 -rwxr-xr-x  1    49 nov.  30 18:19 .gitignore
    4 -rw-rw-rw-  1   211 oct.  26 19:58 .gitlab-ci.yml
    4 drwxr-xr-x  2  4096 oct.  19 18:09 index
    4 -rwxr-xr-x  1   744 nov.  30 18:19 index.rst
    4 -rw-rw-rw-  1  1154 mars  30  2020 Makefile
    4 drwxr-xr-x  2  4096 nov.  30 18:21 meta
    4 drwxr-xr-x  4  4096 nov.  30 18:19 nouvelles
    4 drwxr-xr-x 10  4096 nov.  30 18:19 personnes
    4 -rw-rw-rw-  1  1017 nov.  30 10:11 .pre-commit-config.yaml
    4 -rw-rw-rw-  1   307 oct.  26 20:01 pyproject.toml
    4 -rw-r--r--  1    77 nov.  30 18:19 README.md
    4 -rw-r--r--  1   558 nov.  30 18:19 requirements.txt



pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
